package edu.manju.mimi.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h4>mimi-admin-server</h4>
 * <p>咪咪充电管理程序</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-04-21 20:21
 **/
@SpringBootApplication(scanBasePackages = {
        "edu.manju.mimi.db.config",
        "edu.manju.mimi.db.service",
        "edu.manju.mimi.core.security",
        "edu.manju.mimi.core.wxlogin",
        "edu.manju.mimi.admin",
})
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
