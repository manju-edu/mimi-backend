package edu.manju.mimi.admin.controller;

import edu.manju.mimi.core.R;
import edu.manju.mimi.core.security.JwtService;
import edu.manju.mimi.core.security.RedisService;
import edu.manju.mimi.core.utils.IpUtil;
import edu.manju.mimi.core.wxlogin.*;
import edu.manju.mimi.db.entity.Admin;
import edu.manju.mimi.db.service.IAdminService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * <h4>mimi-admin</h4>
 * <p>咪咪后台登录控制器</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-06 16:04
 **/
@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private IAdminService adminService;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private RedisService redisService;

    @Value("${wxmini.appid}")
    private String appid;

    @Value("${wxmini.secret}")
    private String secret;

    @PostMapping("/login")
    public R<LoginResponse> login(@RequestBody LoginInfo loginInfo, HttpServletRequest request) {
        String code = loginInfo.getCode();
        UserInfo userInfo = loginInfo.getUserInfo();
        if (code == null || userInfo == null) {
            return R.fail("bad parameters");
        }

        String openid = null;
        String sessionKey = null;
        try {
            String json = Code2SessionHelper.fetchJsonString(appid, secret, code);
            Code2Session code2Session = Code2Session.fromJson(json);
            openid = code2Session.getOpenid();
            sessionKey = code2Session.getSessionKey();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sessionKey == null || openid == null) {
            return R.fail("Code2session failed!");
        }

        Admin admin = adminService.selectByOpenId(openid);

        if (admin == null) {
            // first time login: create a user
            admin = new Admin();
            admin.setUsername(openid);
            admin.setPassword(openid);
            admin.setOpenid(openid);
            admin.setAvatar(userInfo.getAvatarUrl());
            admin.setNickname(userInfo.getNickName());
            admin.setGender(userInfo.getGender());
            admin.setStatus((byte) 0);
            admin.setLastLoginTime(LocalDateTime.now());
            admin.setLastLoginIp(IpUtil.getIpAddr(request));
            admin.setSessionKey(sessionKey);

            adminService.save(admin);
        } else {
            admin.setLastLoginTime(LocalDateTime.now());
            admin.setLastLoginIp(IpUtil.getIpAddr(request));
            admin.setSessionKey(sessionKey);

            if (!adminService.updateById(admin)) {
                return R.updatedDataFailed();
            }
        }

        // token sign and save
        Integer userId = admin.getId();
        String token = jwtService.sign(userId);
        redisService.set(token, userId.toString());

        // return login-response
        LoginResponse response = new LoginResponse();
        response.setToken(token);
        response.setUserInfo(userInfo);
        return R.ok(response);
    }
}
