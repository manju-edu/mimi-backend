package edu.manju.mimi.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.manju.mimi.db.entity.Lord;

/**
 * <p>
 * 桩主表 服务类
 * </p>
 *
 * @author Lebei
 * @since 2023-03-02
 */
public interface ILordService extends IService<Lord> {

    Lord selectByOpenId(String openid);
}
