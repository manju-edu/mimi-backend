package edu.manju.mimi.db.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <h4>mimi-db</h4>
 * <p>配置MybatisPlus，如分页等</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-02-24 19:42
 **/
@Configuration
// https://baomidou.com/pages/226c21/#%E9%85%8D%E7%BD%AE
@MapperScan("edu.manju.mimi.db.mapper")
public class MybatisPlusConfig {
    // https://baomidou.com/pages/2976a3/#mybatisplusinterceptor
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}
