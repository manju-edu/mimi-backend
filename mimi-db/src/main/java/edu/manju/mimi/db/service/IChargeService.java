package edu.manju.mimi.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.manju.mimi.db.entity.Charge;

import java.util.List;

/**
 * <p>
 * 充电记录 服务类
 * </p>
 *
 * @author Lebei
 * @since 2023-04-11
 */
public interface IChargeService extends IService<Charge> {
    List<Charge> getChargesByLordId(Integer lordId);

    Charge getUndoneChargeByDevice(Integer deviceId, Byte portNum);
}
