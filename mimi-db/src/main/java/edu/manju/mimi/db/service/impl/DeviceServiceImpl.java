package edu.manju.mimi.db.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.manju.mimi.db.entity.Device;
import edu.manju.mimi.db.mapper.DeviceMapper;
import edu.manju.mimi.db.service.IDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h4>mimi-db</h4>
 * <p>设备表 服务实现类</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-04-10 10:24
 **/
@Service
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements IDeviceService {
    @Autowired
    private DeviceMapper deviceMapper;

    @Override
    public Device selectByLordIdAndName(Integer lordId, String name) {
        return deviceMapper.selectOne(Wrappers.<Device>lambdaQuery()
                .eq(Device::getLordId, lordId)
                .eq(Device::getName, name)
                .last("limit 1"));
    }

    @Override
    public List<Device> getDevicesByLordId(Integer lordId) {
        return deviceMapper.selectList(Wrappers.<Device>lambdaQuery()
                .eq(Device::getLordId, lordId));
    }

    @Override
    public List<Device> getDevicesByLordIdAndSection(Integer lordId, String section) {
        return deviceMapper.selectList(Wrappers.<Device>lambdaQuery()
                .eq(Device::getLordId, lordId)
                .eq(Device::getSection, section));
    }

    @Override
    public Device getDeviceByLordIdAndDeviceId(Integer lordId, Integer deviceId) {
        return deviceMapper.selectOne(Wrappers.<Device>lambdaQuery()
                .eq(Device::getLordId, lordId)
                .eq(Device::getId, deviceId)
                .last("limit 1"));
    }

    @Override
    public boolean removeByByLordIdAndDeviceId(Integer lordId, Integer deviceId) {
        int count = deviceMapper.delete(Wrappers.<Device>lambdaQuery()
                .eq(Device::getLordId, lordId)
                .eq(Device::getId, deviceId));

        return (count > 0);
    }
}
