package edu.manju.mimi.db.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.manju.mimi.db.entity.Lord;
import edu.manju.mimi.db.service.ILordService;
import edu.manju.mimi.db.mapper.LordMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 桩主表 服务实现类
 * </p>
 *
 * @author Lebei
 * @since 2023-03-02
 */
@Service
public class LordServiceImpl extends ServiceImpl<LordMapper, Lord> implements ILordService {
    @Resource
    private LordMapper lordMapper;

    @Override
    public Lord selectByOpenId(String openid) {
        return lordMapper
                .selectOne(
                        Wrappers.<Lord>lambdaQuery()
                                .eq(Lord::getOpenid, openid)
                                .last("limit 1")
                );
    }
}
