package edu.manju.mimi.db.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 桩主表
 * </p>
 *
 * @author Lebei
 * @since 2023-03-02
 */
@Getter
@Setter
@TableName("mimi_lord")
public class Lord implements Serializable {

    private static final long serialVersionUID = 3002924161456402018L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名称
     */
    @TableField("username")
    private String username;

    /**
     * 用户密码
     */
    @TableField("password")
    private String password;

    /**
     * 性别：0 未知， 1男， 1 女
     */
    @TableField("gender")
    private Byte gender;

    /**
     * 生日
     */
    @TableField("birthday")
    private LocalDate birthday;

    /**
     * 最近一次登录时间
     */
    @TableField("last_login_time")
    private LocalDateTime lastLoginTime;

    /**
     * 最近一次登录IP地址
     */
    @TableField("last_login_ip")
    private String lastLoginIp;

    /**
     * 用户昵称或网络名称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 用户手机号码
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 用户头像图片
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 微信登录openid
     */
    @TableField("openid")
    private String openid;

    /**
     * 微信登录会话KEY
     */
    @TableField("session_key")
    private String sessionKey;

    /**
     * 0 可用, 1 禁用, 2 注销
     */
    @TableField("status")
    private Byte status;

    /**
     * 创建时间
     */
    @TableField(value = "add_time", fill = FieldFill.INSERT)
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;
}
