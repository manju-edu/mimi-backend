package edu.manju.mimi.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.manju.mimi.db.entity.Admin;

/**
 * <h4>mimi-backend</h4>
 * <p>管理员表 服务类</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-14 11:08
 **/
public interface IAdminService extends IService<Admin> {
    Admin selectByOpenId(String openid);
}
