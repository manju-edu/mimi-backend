package edu.manju.mimi.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.manju.mimi.db.entity.Lord;

/**
 * <p>
 * 桩主表 Mapper 接口
 * </p>
 *
 * @author Lebei
 * @since 2023-03-02
 */
public interface LordMapper extends BaseMapper<Lord> {

}
