package edu.manju.mimi.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.manju.mimi.db.entity.Charge;

/**
 * <p>
 * 充电记录 Mapper 接口
 * </p>
 *
 * @author Lebei
 * @since 2023-04-11
 */
public interface ChargeMapper extends BaseMapper<Charge> {

}
