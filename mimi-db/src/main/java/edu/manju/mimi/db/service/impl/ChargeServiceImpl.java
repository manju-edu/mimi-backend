package edu.manju.mimi.db.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.manju.mimi.db.entity.Charge;
import edu.manju.mimi.db.mapper.ChargeMapper;
import edu.manju.mimi.db.service.IChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 充电记录 服务实现类
 * </p>
 *
 * @author Lebei
 * @since 2023-04-11
 */
@Service
public class ChargeServiceImpl extends ServiceImpl<ChargeMapper, Charge> implements IChargeService {
    @Autowired
    private ChargeMapper chargeMapper;

    @Override
    public List<Charge> getChargesByLordId(Integer lordId) {
        return chargeMapper.selectList(Wrappers.<Charge>lambdaQuery()
                .eq(Charge::getLordId, lordId));
    }

    @Override
    public Charge getUndoneChargeByDevice(Integer deviceId, Byte portNum) {
        return chargeMapper.selectOne(Wrappers.<Charge>lambdaQuery()
                .eq(Charge::getStatus, 0)
                .eq(Charge::getDeviceId, deviceId)
                .eq(Charge::getPort, portNum)
                .last("limit 1")
        );
    }
}
