package edu.manju.mimi.db.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.manju.mimi.db.entity.Admin;
import edu.manju.mimi.db.mapper.AdminMapper;
import edu.manju.mimi.db.service.IAdminService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <h4>mimi-db</h4>
 * <p>管理员表 服务实现类</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-14 11:10
 **/
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {
    @Resource
    private AdminMapper adminMapper;

    @Override
    public Admin selectByOpenId(String openid) {
        return adminMapper.selectOne(
                Wrappers.<Admin>lambdaQuery().eq(Admin::getOpenid, openid));
    }
}
