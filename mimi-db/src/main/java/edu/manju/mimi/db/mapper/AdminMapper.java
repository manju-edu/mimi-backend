package edu.manju.mimi.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.manju.mimi.db.entity.Admin;

/**
 * <h4>mimi-db</h4>
 * <p>管理员表 Mapper 接口</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-14 11:06
 **/
public interface AdminMapper extends BaseMapper<Admin> {
}
