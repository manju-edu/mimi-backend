package edu.manju.mimi.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.manju.mimi.db.entity.User;

/**
 * <h4>mimi-backend</h4>
 * <p>充电用户(车主) 服务类</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-06 15:18
 **/
public interface IUserService extends IService<User> {
    User selectByOpenId(String openid);
}
