package edu.manju.mimi.db.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.manju.mimi.db.entity.User;
import edu.manju.mimi.db.mapper.UserMapper;
import edu.manju.mimi.db.service.IUserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <h4>mimi-backend</h4>
 * <p>充电用户 服务实现类</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-06 15:20
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public User selectByOpenId(String openid) {
        return userMapper
                .selectOne(
                        Wrappers.<User>lambdaQuery()
                                .eq(User::getOpenid, openid)
                                .last("limit 1"));
    }
}
