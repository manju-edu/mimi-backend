package edu.manju.mimi.db.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <h4>mimi-db</h4>
 * <p>设备</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-04-10 10:11
 **/
@Getter
@Setter
@TableName("mimi_device")
public class Device implements Serializable {
    private static final long serialVersionUID = -10493719847103598L;

    /**
     * 设备ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 设备名称
     */
    @TableField("name")
    private String name;

    /**
     * 详细安装地址
     */
    @TableField("address")
    private String address;

    /**
     * 经度
     */
    @TableField("longitude")
    private Double longitude;

    /**
     * 纬度
     */
    @TableField("latitude")
    private Double latitude;

    /**
     * 片区
     */
    @TableField("section")
    private String section;

    /**
     * 出厂编号
     */
    @TableField("vendor")
    private String vendor;

    /**
     * 端口数
     */
    @TableField("port_num")
    private Byte portNum;

    /**
     * 桩主ID
     */
    @TableField("lord_id")
    private Integer lordId;


    /**
     * 设备是否创建：0-未创建，1-已创建
     */
    @TableField("iot_created")
    private Byte iotCreated;
}
