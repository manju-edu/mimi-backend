package edu.manju.mimi.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.manju.mimi.db.entity.Device;

import java.util.List;

/**
 * <h4>mimi-db</h4>
 * <p>设备表 服务类</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-04-10 10:21
 **/
public interface IDeviceService extends IService<Device> {
    Device selectByLordIdAndName(Integer lordId, String name);

    List<Device> getDevicesByLordId(Integer lordId);

    List<Device> getDevicesByLordIdAndSection(Integer lordId, String section);

    Device getDeviceByLordIdAndDeviceId(Integer lordId, Integer deviceId);

    boolean removeByByLordIdAndDeviceId(Integer lordId, Integer deviceId);
}
