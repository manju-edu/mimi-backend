package edu.manju.mimi.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.manju.mimi.db.entity.User;

/**
 * <h4>mimi-backend</h4>
 * <p>充电用户(车主) Mapper 接口</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-06 15:15
 **/
public interface UserMapper extends BaseMapper<User> {

}
