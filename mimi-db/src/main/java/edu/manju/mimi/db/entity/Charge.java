package edu.manju.mimi.db.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 充电记录
 * </p>
 *
 * @author Lebei
 * @since 2023-04-11
 */
@Getter
@Setter
@TableName("mimi_charge")
public class Charge implements Serializable {

    private static final long serialVersionUID = 706339371161363187L;

    /**
     * 订单编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 创建时间
     */
    @TableField("start_time")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @TableField("end_time")
    private LocalDateTime endTime;

    /**
     * 充电时长
     */
    @TableField("duration")
    private Double duration;

    /**
     * 车主ID
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 桩主ID
     */
    @TableField("lord_id")
    private Integer lordId;

    /**
     * 设备ID
     */
    @TableField("device_id")
    private Integer deviceId;

    /**
     * 设备名称
     */
    @TableField("device_name")
    private String deviceName;

    /**
     * 端口号
     */
    @TableField("port")
    private Byte port;

    /**
     * 收费类型：0-无，1-计时
     */
    @TableField("fee_type")
    private Byte feeType;

    /**
     * 收费标准(元/小时)
     */
    @TableField("fee_rate")
    private Double feeRate;

    /**
     * 结算功率(W)
     */
    @TableField("power")
    private Double power;

    /**
     * 结算金额(元)
     */
    @TableField("amount")
    private Double amount;

    /**
     * 订单状态：0-充电中，1-已结算
     */
    @TableField("status")
    private Byte status;
}
