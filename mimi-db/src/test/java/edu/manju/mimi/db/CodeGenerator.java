package edu.manju.mimi.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;

import java.util.Collections;

// https://baomidou.com/pages/779a6e/#%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8
public class CodeGenerator {
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/mimi-db?characterEncoding=UTF-8&serverTimezone=Asia/Shanghai";
        String username = "root";
        String password = "12345678";

        String author = "Lebei";
        String moduleName = "lord";

        String currPath = System.getProperty("user.dir");
        String path = currPath + "/src/main/java";
        String mapperLocation = currPath + "/src/main/resources/mapper/" + moduleName;

        String tables = "mimi_charge";
        String prefix = "mimi_";
        String pkgPrefix = "edu.manju.mimi.lordserver";

        FastAutoGenerator.create(url, username, password)
                .globalConfig(builder -> {
                    builder.author(author) //设置作者
                            // .enableSwagger() //开启 swagger 模式
                            .outputDir(path); //指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent(pkgPrefix) //设置父包名
                            .moduleName(moduleName) //设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, mapperLocation)); //设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude(tables) //设置需要生成的表名
                            .addTablePrefix(prefix) //设置过滤表前缀
                            .entityBuilder() //实体类策略配置
//                            .enableFileOverride()
                            .addTableFills(new Column("add_time", FieldFill.INSERT)) //自动填充配置
                            .addTableFills(new Column("update_time", FieldFill.INSERT_UPDATE))
                            .enableLombok() //开启lombok
                            .logicDeleteColumnName("deleted") //说明逻辑删除是哪个字段
                            .enableTableFieldAnnotation() //属性加上注解说明
                            .controllerBuilder() //controller策略配置
//                            .enableFileOverride()
                            .enableRestStyle() //开启RestController注解
                            .mapperBuilder() //mapper策略配置
//                            .enableFileOverride()
                            .serviceBuilder(); //service策略配置
//                            .enableFileOverride();

                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
