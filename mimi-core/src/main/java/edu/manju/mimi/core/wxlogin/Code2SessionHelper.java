package edu.manju.mimi.core.wxlogin;

import cn.hutool.http.HttpUtil;

/**
 * <h4>mimi-core</h4>
 * <p>提供微信code2session服务</p>
 * 参见[微信开放文档](https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-login/code2Session.html)
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-01 15:39
 **/
public class Code2SessionHelper {
    public static String fetchJsonString(String appid, String secret, String code) {
        String url = getCode2sessionUrl(appid, secret, code);
        return HttpUtil.get(url);
    }

    private final static String CODE2SESSION_URL
            = "https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type=authorization_code";

    private static String getCode2sessionUrl(String appid, String secret, String code) {
        return CODE2SESSION_URL.replace("{0}", appid).replace("{1}", secret).replace("{2}", code);
    }
}
