package edu.manju.mimi.core.security;

import java.lang.annotation.*;

/**
 * <h4>mimi-core</h4>
 * <p>参数注解,获取当前用户ID</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-03 22:15
 **/
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurrentUserId {
}
