package edu.manju.mimi.core.wxlogin;

import lombok.Data;

/**
 * <h4>mimi-core</h4>
 * <p>完成微信授权登录所需要的信息</p>
 * 参见[微信开放文档](https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/login.html)
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-01 15:23
 **/
@Data
public class LoginInfo {
    private String code;
    private UserInfo userInfo;
}
