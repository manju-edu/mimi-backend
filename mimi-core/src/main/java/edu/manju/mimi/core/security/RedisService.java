package edu.manju.mimi.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * <h4>mimi-core</h4>
 * <p>Token的存储服务</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-03 09:48
 **/
@Service
public class RedisService {
    private final static String TOKEN_PREFIX = "token-key-";
    @Autowired
    private StringRedisTemplate redisTemplate;

    public void set(String token, String value) {
        redisTemplate.opsForValue().set(TOKEN_PREFIX + token, value, 7, TimeUnit.DAYS);
    }

    public String get(String token) {
        return redisTemplate.opsForValue().get(TOKEN_PREFIX + token);
    }
}
