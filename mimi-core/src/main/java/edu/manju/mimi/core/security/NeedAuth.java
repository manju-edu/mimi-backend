package edu.manju.mimi.core.security;

import java.lang.annotation.*;

/**
 * <h4>mimi-core</h4>
 * <p>标记需要登录才能访问的方法</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-02-24 20:53
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NeedAuth {
}
