package edu.manju.mimi.core.wxlogin;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

/**
 * <h4>mimi-core</h4>
 * <p>存放微信code2session的结果</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-02 14:08
 **/
@Data
public class Code2Session {
    // https://cloud.tencent.com/developer/article/2056595
    @JSONField(name = "session_key")
    private String sessionKey;
    private String openid;
    private String unionid;

    public static Code2Session fromJson(String json) {
        return JSON.parseObject(json, Code2Session.class);
    }
}
