package edu.manju.mimi.core.wxlogin;

import lombok.Data;

/**
 * <h4>mimi-core</h4>
 * <p>wx.getUserInfo获取的用户资料</p>
 * 参见 [微信开发文档](https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html)
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-01 14:16
 **/
@Data
public class UserInfo {
    private String nickName;
    private String avatarUrl;
    private String country;
    private String province;
    private String city;
    private String language;
    private Byte gender;
}
