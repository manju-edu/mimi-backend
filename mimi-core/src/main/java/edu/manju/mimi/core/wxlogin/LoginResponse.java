package edu.manju.mimi.core.wxlogin;

import lombok.Data;

/**
 * <h4>mimi-core</h4>
 * <p>登录响应，包含token和用户信息</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-02 17:24
 **/
@Data
public class LoginResponse {
    private String token;
    private UserInfo userInfo;
}
