package edu.manju.mimi.lord.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * <h4>mimi-lord-server</h4>
 * <p>Onenet的配置信息</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-03 16:59
 **/
@Configuration
@ConfigurationProperties(prefix = "onenet")
@Data
public class OnenetConfig {
    private String userId;
    private String productId;
    private String accessKey;
}
