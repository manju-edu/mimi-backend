package edu.manju.mimi.lord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h4>mimi-lord</h4>
 * <p>咪咪桩主服务端启动程序</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-02-24 20:11
 **/
@SpringBootApplication(scanBasePackages = {
        "edu.manju.mimi.db.config",
        "edu.manju.mimi.db.service",
        "edu.manju.mimi.core.security",
        "edu.manju.mimi.core.wxlogin",
        "edu.manju.mimi.lord",
})
public class LordApplication {
    public static void main(String[] args) {
        SpringApplication.run(LordApplication.class, args);
    }
}
