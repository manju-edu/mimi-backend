package edu.manju.mimi.lord.controller;

import edu.manju.mimi.core.R;
import edu.manju.mimi.core.security.CurrentUserId;
import edu.manju.mimi.core.security.NeedAuth;
import edu.manju.mimi.db.service.IChargeService;
import edu.manju.mimi.db.entity.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 充电记录 前端控制器
 * </p>
 *
 * @author Lebei
 * @since 2023-04-11
 */
@RestController
@RequestMapping("/charge")
public class ChargeController {
    @Autowired
    private IChargeService chargeService;

    @GetMapping("/search/{searchDate}")
    @NeedAuth
    public R<HashMap> getChargesOfLord(@CurrentUserId Integer userId,
                                       @PathVariable String searchDate) {
        List<Charge> charges = chargeService.getChargesByLordId(userId);
        double amount = charges.stream()
                .map(Charge::getAmount)
                .reduce(0.0, Double::sum);

        HashMap<String, Object> ret = new HashMap<>();
        ret.put("amount", amount);
        ret.put("list", charges);

        return R.ok(ret);
    }

    @GetMapping("/list/{id}")
    @NeedAuth
    public R<Charge> getChargeById(@CurrentUserId Integer userId,
                                   @PathVariable Integer id) {
        Charge charge = chargeService.getById(id);
        return R.ok(charge);
    }
}
