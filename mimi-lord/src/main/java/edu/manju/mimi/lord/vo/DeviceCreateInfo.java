package edu.manju.mimi.lord.vo;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * <h4>mimi-lord-server</h4>
 * <p>设备创建所需的资料</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-03-03 17:18
 **/
@Data
public class DeviceCreateInfo {
    @NotBlank(message = "设备名不能为空")
    private String name;
    private String address;
    private String section;
    private double longitude;
    private double latitude;
    private String vendor;
    private Byte portNum;
}
