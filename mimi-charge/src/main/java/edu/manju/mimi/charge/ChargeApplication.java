package edu.manju.mimi.charge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h4>mimi-charge</h4>
 * <p>咪咪充电服务端启动程序</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-04-21 20:11
 **/
@SpringBootApplication(scanBasePackages = {
        "edu.manju.mimi.db.config",
        "edu.manju.mimi.db.service",
        "edu.manju.mimi.core.security",
        "edu.manju.mimi.core.wxlogin",
        "edu.manju.mimi.charge",
})
public class ChargeApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChargeApplication.class, args);
    }
}
