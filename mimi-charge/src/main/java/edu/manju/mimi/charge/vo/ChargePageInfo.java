package edu.manju.mimi.charge.vo;

import lombok.Data;

/**
 * <h4>mimi-charge</h4>
 * <p>充电页面所需的资料</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-08 20:37
 **/
@Data
public class ChargePageInfo {
    private String deviceName;
    private String status;
    private Double balance;
    private boolean deficit;
}
