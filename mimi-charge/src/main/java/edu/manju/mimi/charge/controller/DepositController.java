package edu.manju.mimi.charge.controller;

import edu.manju.mimi.charge.vo.DepositCreateInfo;
import edu.manju.mimi.charge.vo.DepositPageInfo;
import edu.manju.mimi.core.R;
import edu.manju.mimi.core.security.CurrentUserId;
import edu.manju.mimi.core.security.NeedAuth;
import edu.manju.mimi.db.entity.User;
import edu.manju.mimi.db.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <h4>mimi-backend</h4>
 * <p>充值 控制器</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-09 15:14
 **/
@RestController
@RequestMapping("/deposit")
public class DepositController {
    @Autowired
    private IUserService userService;

    @NeedAuth
    @GetMapping("/queryBalance")
    public R<DepositPageInfo> queryBalance(@CurrentUserId Integer userId) {
        User user = userService.getById(userId);

        DepositPageInfo depositPageInfo = new DepositPageInfo();
        depositPageInfo.setBalance(user.getBalance());

        return R.ok(depositPageInfo);
    }

    @NeedAuth
    @PostMapping("/create")
    public R<String> createDeposit(@CurrentUserId Integer userId,
                                   @RequestBody DepositCreateInfo info) {
        System.out.println("amount: " + info.getAmount());
        return R.ok("success");
    }
}
