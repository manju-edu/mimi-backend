package edu.manju.mimi.charge.vo;

import lombok.Data;

/**
 * <h4>mimi-charge</h4>
 * <p>订单创建时所需的资料</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-07 09:11
 **/
@Data
public class ChargeCreateInfo {
    private Integer deviceId;

    private Byte portNum;

    private Byte duration;
}
