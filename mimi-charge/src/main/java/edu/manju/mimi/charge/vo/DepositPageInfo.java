package edu.manju.mimi.charge.vo;

import lombok.Data;

/**
 * <h4>mimi-charge</h4>
 * <p>充值页面所需的资料</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-09 15:11
 **/
@Data
public class DepositPageInfo {
    private Double balance;
}
