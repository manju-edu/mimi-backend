package edu.manju.mimi.charge.controller;

import edu.manju.mimi.charge.vo.ChargePageInfo;
import edu.manju.mimi.charge.vo.ChargeCreateInfo;
import edu.manju.mimi.core.R;
import edu.manju.mimi.core.security.CurrentUserId;
import edu.manju.mimi.core.security.NeedAuth;
import edu.manju.mimi.db.entity.Device;
import edu.manju.mimi.db.entity.Charge;
import edu.manju.mimi.db.entity.User;
import edu.manju.mimi.db.service.IDeviceService;
import edu.manju.mimi.db.service.IChargeService;
import edu.manju.mimi.db.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <h4>mimi-charge</h4>
 * <p>充电记录 前端控制器</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-07 09:02
 **/
@RestController
@RequestMapping("/charge")
public class ChargeController {
    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private IChargeService chargeService;

    @Autowired
    private IUserService userService;

    @NeedAuth
    @PostMapping("/create")
    public R<String> createCharge(@CurrentUserId Integer userId,
                                  @RequestBody ChargeCreateInfo chargeInfo) {
        Integer deviceId = chargeInfo.getDeviceId();
        Byte portNum = chargeInfo.getPortNum();

        Device device = deviceService.getById(deviceId);
        if (device == null || device.getPortNum() < portNum) {
            return R.fail("没有设备或端口");
        }

        Charge charge = chargeService.getUndoneChargeByDevice(deviceId, portNum);
        if (charge != null) {
            return R.fail("设备已占用！");
        }

        charge = new Charge();
        charge.setStartTime(LocalDateTime.now());
        charge.setUserId(userId);
        charge.setDeviceId(deviceId);
        charge.setDeviceName(device.getName());
        charge.setLordId(device.getLordId());
        charge.setPort(portNum);
        charge.setDuration(chargeInfo.getDuration().doubleValue());
        charge.setFeeType((byte) 1);
        charge.setFeeRate(0.35);
        charge.setPower(220.00);

        boolean save = chargeService.save(charge);
        if (!save) {
            return R.fail("不能保存订单！");
        }

        return R.ok("success");
    }

    @NeedAuth
    @GetMapping("/queryChargeInfo/{deviceId}/{portNum}")
    public R<?> queryCharge(@CurrentUserId Integer userId,
                            @PathVariable Integer deviceId,
                            @PathVariable Byte portNum) {
        ChargePageInfo chargePageInfo = new ChargePageInfo();

        Device device = deviceService.getById(deviceId);
        if (device == null || device.getPortNum() < portNum) {
            return R.fail("没有设备或端口");
        }

        Charge charge = chargeService.getUndoneChargeByDevice(deviceId, portNum);
        if (charge != null) {
            return R.fail("设备已占用！");
        }

        User user = userService.getById(userId);
        if (user == null) {
            return R.fail("用户不存在");
        }

        chargePageInfo.setDeviceName(device.getName());
        chargePageInfo.setBalance(user.getBalance());
        chargePageInfo.setStatus("空闲");
        chargePageInfo.setDeficit(false);

        return R.ok(chargePageInfo);
    }
}
