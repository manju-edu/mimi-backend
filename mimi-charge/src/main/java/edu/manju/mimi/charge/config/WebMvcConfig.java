package edu.manju.mimi.charge.config;

import edu.manju.mimi.core.security.LoginInterceptor;
import edu.manju.mimi.core.security.LoginUserIdResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * <h4>mimi-lord-server</h4>
 * <p>配置WebMvc，如登录拦截器等</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-02-24 21:24
 **/
@Component
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new LoginUserIdResolver());
    }
}
