DROP TABLE IF EXISTS `mimi_admin`;
CREATE TABLE `mimi_admin` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(63) NOT NULL COMMENT '管理员',
  `password` varchar(63) NOT NULL DEFAULT '' COMMENT '密码',
  `gender` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '性别：0 未知， 1男， 1 女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近一次登录时间',
  `last_login_ip` varchar(63) NOT NULL DEFAULT '' COMMENT '最近一次登录IP地址',
  `nickname` varchar(63) NOT NULL DEFAULT '' COMMENT '用户昵称或网络名称',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像图片',
  `openid` varchar(63) NOT NULL DEFAULT '' COMMENT '微信登录openid',
  `session_key` varchar(100) NOT NULL DEFAULT '' COMMENT '微信登录会话KEY',
  `status` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '0 可用, 1 禁用, 2 注销',
  `add_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`username`),
  KEY `idx_user_openid` (`openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

DROP TABLE IF EXISTS `mimi_lord`;
CREATE TABLE `mimi_lord` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(63) NOT NULL COMMENT '桩主名',
  `password` varchar(63) NOT NULL DEFAULT '' COMMENT '密码',
  `gender` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '性别：0 未知， 1男， 1 女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近一次登录时间',
  `last_login_ip` varchar(63) NOT NULL DEFAULT '' COMMENT '最近一次登录IP地址',
  `nickname` varchar(63) NOT NULL DEFAULT '' COMMENT '用户昵称或网络名称',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像图片',
  `openid` varchar(63) NOT NULL DEFAULT '' COMMENT '微信登录openid',
  `session_key` varchar(100) NOT NULL DEFAULT '' COMMENT '微信登录会话KEY',
  `status` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '0 可用, 1 禁用, 2 注销',
  `add_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`username`),
  KEY `idx_user_openid` (`openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='桩主表';

DROP TABLE IF EXISTS `mimi_user`;
CREATE TABLE `mimi_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(63) NOT NULL COMMENT '用户名',
  `password` varchar(63) NOT NULL DEFAULT '' COMMENT '密码',
  `gender` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '性别：0 未知， 1男， 1 女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近一次登录时间',
  `last_login_ip` varchar(63) NOT NULL DEFAULT '' COMMENT '最近一次登录IP地址',
  `nickname` varchar(63) NOT NULL DEFAULT '' COMMENT '用户昵称或网络名称',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像图片',
  `openid` varchar(63) NOT NULL DEFAULT '' COMMENT '微信登录openid',
  `session_key` varchar(100) NOT NULL DEFAULT '' COMMENT '微信登录会话KEY',
  `balance` decimal(5,2) unsigned DEFAULT '0.00' COMMENT '账户余额(元)',
  `status` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '0 可用, 1 禁用, 2 注销',
  `add_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`username`),
  KEY `idx_user_openid` (`openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

DROP TABLE IF EXISTS `mimi_device`;
CREATE TABLE `mimi_device` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '设备ID',
  `name` varchar(40) NOT NULL COMMENT '设备名称',
  `address` varchar(120) NOT NULL COMMENT '详细安装地址',
  `longitude` decimal(10,6) NOT NULL COMMENT '经度',
  `latitude` decimal(10,6) NOT NULL COMMENT '纬度',
  `section` varchar(20) NOT NULL COMMENT '片区',
  `vendor` varchar(40) NOT NULL COMMENT '出厂编号',
  `port_num` tinyint DEFAULT '10' COMMENT '端口数',
  `lord_id` int unsigned NOT NULL COMMENT '桩主ID',
  `iot_created` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '设备是否创建成功：0-未创建，1-已创建',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_LORD_ID_NAME` (`lord_id`,`name`) USING BTREE COMMENT '设备名在每个桩主下是唯一的'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='设备表'

DROP TABLE IF EXISTS `mimi_charge`;
CREATE TABLE `mimi_charge` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '订单编号',
  `start_time` datetime NOT NULL COMMENT '创建时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `duration` decimal(5,2) unsigned DEFAULT '0.00' COMMENT '充电时长',
  `user_id` int unsigned DEFAULT NULL COMMENT '车主ID',
  `lord_id` int unsigned DEFAULT NULL COMMENT '桩主ID',
  `device_id` int unsigned DEFAULT NULL COMMENT '设备ID',
  `device_name` varchar(40) NOT NULL COMMENT '设备名称',
  `port` tinyint unsigned DEFAULT NULL COMMENT '端口号',
  `fee_type` tinyint unsigned DEFAULT '1' COMMENT '收费类型：0-无，1-计时',
  `fee_rate` decimal(5,2) unsigned DEFAULT '0.00' COMMENT '收费标准(元/小时)',
  `power` decimal(5,2) unsigned DEFAULT '0.00' COMMENT '结算功率(W)',
  `amount` decimal(5,2) unsigned DEFAULT '0.00' COMMENT '结算金额(元)',
  `status` tinyint unsigned DEFAULT '0' COMMENT '订单状态：0-充电中，1-已结算',
  PRIMARY KEY (`id`),
  KEY `IDX_START_TIME` (`start_time`,`lord_id`) /*!80000 INVISIBLE */,
  KEY `IDX_LORD_ID` (`lord_id`,`start_time`),
  KEY `IDX_STATUS` (`status`, `device_id`, `port`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='充电记录'
