package edu.manju.mimi;

import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <h4>mimi-helper</h4>
 * <p>用于配置文件脱敏</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-07 11:12
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class JasyptEncryptorTests {
    @Autowired
    private StringEncryptor encryptor;

    @Test
    public void testEncrypt() {
//        String chargeSecret = encryptor.encrypt("");
//        System.out.println("charge secret: " + chargeSecret);

//        String lordSecret = encryptor.encrypt("");
//        System.out.println("  lord secret: " + lordSecret);

        String adminSecret = encryptor.encrypt("");
        System.out.println("  admin secret: " + adminSecret);

//        String onenetSecret = "";
//        System.out.println("onenet secret: " + onenetSecret);
//
//        String onenetEncrypt = encryptor.encrypt(onenetSecret);
//        System.out.println("   onenet enc: " + onenetEncrypt);
//
//        String onenetDecrypt = encryptor.decrypt(onenetEncrypt);
//        System.out.println("   onenet dec: " + onenetDecrypt);
    }
}
