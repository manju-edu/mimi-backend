package edu.manju.mimi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h4>mimi-helper</h4>
 * <p>工具</p>
 *
 * @author : Lebei(lebei163@163.com)
 * @date : 2023-05-07 10:30
 **/
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}